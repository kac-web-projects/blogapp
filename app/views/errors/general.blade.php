@extends('layouts.errors')
@section('title', 'General Error')

@section('content')

<div class="grid">

    <div class="row cells12" style="margin-top: 80px">
		
		<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"> Internal Error</h2>

        <a href="#" id="controller">Show More</a>

        <div class="row cell12" id="advance" style="display:none">
        	{{ $error }}
        </div>

	</div>

</div>
@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

        $('#controller').on('click',function() {
            
            $('#advance').toggle();
        });

	});

</script>
@stop






