@extends('layouts.default')
@section('title', 'Documents')

@section('content')
		
<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"> </a> View Document</h2>

<div class="row cell colspan8 offset2">
	<embed src="{{ URL::to('/') }}/manual/User.pdf">
</div>

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop