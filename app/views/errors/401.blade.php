@extends('layouts.errors')
@section('title', 'General Error')

@section('content')

<div class="grid">

    <div class="row cells12" style="margin-top: 80px">
		
		<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Access Denied - 401</h2>

        <div class="row cell12">
            <img src="#">
        </div>

	</div>

</div>
@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop






