@extends('layouts.default')
@section('title', 'Edit User')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Edit User</h2>

<!--  Show Errors if applicable -->
<div class="row cells6">
	@if ($errors->has())

    <div class="block-shadow-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>        
        @endforeach
    </div>
    
    @endif
</div>

<div class="row cells6">
    	{{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PATCH')) }}
	
	<!--  UserName -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('username','Username') }}

			<div class="input-control text full-size">
				{{ Form::text('username',null,array('classK' => 'mif-2x')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>

			</div>

		</div>
	</div>

	<!--  First Name -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('first_name','First Name') }}

			<div class="input-control text full-size">
				{{ Form::text('first_name',null,array('classK' => 'mif-2x')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>

			</div>

		</div>
	</div>


	<!--  Last Name -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('last_name','Last Name') }}

			<div class="input-control text full-size">
				{{ Form::text('last_name',null,array('placeHolder' => 'Last name','class' => '')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>
				
			</div>
	
		</div>
	</div>

	<!--  Password -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('password','New Password') }}

			<div class="input-control text full-size">
				{{ Form::password('password',null,array('class' => '')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>
				
			</div>

		</div>
	</div>

	<!--  Confirm Password -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('password_confirm','Confirm Password') }}

			<div class="input-control text full-size">
				{{ Form::password('password_confirm',null,array('class' => '')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>

			</div>

		</div>
	</div>

	<!--  Email -->
	<div class="row cells2">
		
		<div class="cell">
			{{ Form::label('email','Email') }}

			<div class="input-control select full-size">
				{{ Form::text('email',null,array('placeHolder' => 'Email','class' => '')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>
			</div>

		</div>
	</div>

	<!--  User Role -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('role_id', 'Role')}}
			
			<div class="input-control select full-size">
				
				{{ Form::select('role_id', $roles)}}
			</div>

		</div>
	</div>
	
	<!--  Active -->
	<div class="row cells2">
		
		<label class="input-control checkbox full-check">
		    <input type="checkbox" checked>
		    <span class="caption">Active</span>
		    <span class="check"></span>
		</label>
	</div>

</div>
<!-- !cell8 -->

<div class="row cells4">

    <div class="row cell colspan4 offset5" style="margin-top: 50px">
		<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
 		{{ Form::submit('Update', array('class' => 'button primary')) }}
	</div>

</div><!-- !cell4 -->

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	// function notifyOnErrorInput(input){
 //        var message = input.data('validateHint');
 //        $.Notify({
 //            caption: 'Error',
 //            content: message,
 //            type: 'alert'
 //        });
 //    }


	});

</script>
@stop






