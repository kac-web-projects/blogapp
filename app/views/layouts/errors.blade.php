<!doctype html>
<html>
<head>
    @include('includes.head')
</head>

<body class="metro">

    <header class="bg-dark fg-light" data-load="header.html">
        @include('includes.header')
    </header>

    <div class="container">

        <div class="grid">

            <div class="row cells12">

                <!-- main content -->
                <div id="content" class="cell colspan11">
                    @yield('content')
                </div>

            </div><!-- !row cells12 -->


        
        </div><!-- grid -->
        <div class="row cells12">

                <footer class="row cells12">
                    @include('includes.footer')
                </footer>
            </div>

    </div><!-- container -->
   
    @include('includes.script')
    <!-- JS Section -->
    @yield('script')

</body>
</html>