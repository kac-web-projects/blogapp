<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class RolesController extends \BaseController {

	// define validation rules
	public static $rules = array(
			'name'  => 'required|unique:roles',
            'description' => 'required'
        );

	/**
	 * Admin Filter
	 *
	 */
	public function __construct()
    {
        $this->beforeFilter('auth.admin', array('only' => array('index', 'create', 'store', 'edit','update','destroy')));
    }


	/**
	 * Listing Roles
	 *
	 * @return Response
	 */
	public function index()
	{
		Log::info("RolesController::index()");

		$roles = Roles::all();

		return View::make('roles.index')->with('roles', $roles);
	}


	/**
	 * Show the form for creating a new Role.
	 *
	 * @return Response
	 */
	public function create()
	{
		Log::info("RolesController::create()");

		return View::make('roles.create');
	}


	/**
	 * Store a newly created Role in DB.
	 *
	 * @return Response
	 */
	public function store()
	{
		Log::info("RolesController::store()");

		$in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
		
			Log::info("Validation : Failed while Storing Role");

			// track the error messages from the validator
			Log::debug($validator->messages());

			// notify
	        Session::flash('type', 'Warning');
            Session::flash('caption', 'Role');
            Session::flash('content', 'Validation Failed');

            // redirect
            return Redirect::to('role/create')
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // store
            $role               = new Roles();
            $role->name         = $in['name'];
            $role->description  = $in['description'];
			$role->save();

			// notify
	        Session::flash('type', 'Notify');
            Session::flash('caption', 'Role');
            Session::flash('content', 'Created Successfully');
    
            // redirect
            return Redirect::to("role");
        }
	}

	/**
	 * Show the form for editing the specified Role.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		Log::info("RolesController::edit($id)");

		// retrieve particular role
        $role = Roles::findOrFail($id);

        // show the view and pass the post to it
        return View::make('roles.edit')
            ->with('role', $role);
	}


	/**
	 * Update the specified Role in DB.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		Log::info("RolesController::update($id)");

		$in = Input::all();
        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
		
			Log::debug("Validation : Failed while Updating Role-Id : $id");

			// track the error messages from the validator
			Log::debug($validator->messages());

			// notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Role');
            Session::flash('content', 'Validation Failed');
            
            // redirect
            return Redirect::to("role/$id/edit")
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // update
            $role               = Roles::findOrFail($id);
            $role->name         = $in['name'];
            $role->description  = $in['description'];
			$role->save();

			// notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Role');
            Session::flash('content', 'Updated Successfully');

            // redirect
            return Redirect::to("role/$id/edit");
        }

	}


	/**
	 * Remove the specified Role from DB.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	
	/**
	 * Defining A Catch-All Method
	 *
	 * @param  Array
	 * @return Response
	 */
	public function missingMethod($parameters = array())
	{
		Log::info("RolesController::missingMethod()");

		return 'missingMethod';
	}
}