-- LookUps - Roles
INSERT INTO roles (name, description) VALUES ("admin","All operations permitted");
INSERT INTO roles (name, description) VALUES ("author","Limitted operations permitted");
INSERT INTO roles (name, description) VALUES ("reader","Few operations permitted");


-- Demo - Users
INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `role_id`, `remember_token`, `active`)
VALUES
	(1, 'media', '$2y$10$k0Q7bYYFZiAdusCGpYwzMuV42kNYkw7zpEI/6tjzo1vQz0xxvz3e2', 'Media', 'Master', 'media@gmail.com', 1, 'Cj38iE8iu1YHaDtvjdX7F6xTmNugvJwxqHjRf1JZXUulJINPT6zT8m1Bb0Uj', 1);
INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `role_id`, `remember_token`, `active`)
VALUES
	(2, 'norman', '$2y$10$XNDV/.JGIxP3bINzIk038uXU7zZo1PndXFuuYpMQdUPJwqekgSX3G', 'Norman', 'Cote', 'norman@gmail.com', 2, NULL, 1);
INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `role_id`, `remember_token`, `active`)
VALUES
	(3, 'chris', '$2y$10$q/QqvsrhstDzyVsT5t8ePegnodINbdqXClFgaqweoE9mPRXoY61bS', 'Chris', 'Roger', 'chris@gmail.com', 3, NULL, 1);
