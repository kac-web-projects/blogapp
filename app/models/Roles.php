<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class Roles extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles';


	/**
	 * Timestamps field such as created_at and updated_at if required.
	 *
	 * @var boolean
	 */
	public $timestamps = false;

}