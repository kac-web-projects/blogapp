<?php

//Home
Route::resource('/', 'BlogController', array('only' => array('index')));
Route::get('most-commented', 'BlogController@mostCommented');
Route::get('most-popular', 'BlogController@mostPopular');
Route::get('most-recent', 'BlogController@mostRecent');

//Filter Added Already OR not Applicable
Route::resource('post', 'PostsController');
Route::resource('role', 'RolesController');
Route::resource('user', 'UserController');
Route::get('comment/operations', 'CommentService@operations');

Route::group(['before' => 'auth'], function () {

    Route::get('myposts', 'PostsController@userPosts');

    //Service
    Route::get('services', 'Services@delete');
    Route::get('comment/post', 'CommentService@addComment');
    Route::get('comment/get', 'CommentService@getComments');

    //System Routes
    Route::get('routes', function () {

        Log::info("Routes::routes");

        $routes = Route::getRoutes();
        return View::make('admin.view_routes')->with('routes', $routes);
    });

    //Dashboard
    Route::get('dashboard', array('uses' => 'LoginController@showDashboard'));

});

/*
|--------------------------------------------------------------------------
| Authentication URIs
|--------------------------------------------------------------------------
| Login, Logout, Reset Password
|
 */
Route::get('login', array('uses' => 'LoginController@showLogin'));
Route::post('login', array('uses' => 'LoginController@doLogin'));
Route::get('logout', array('uses' => 'LoginController@doLogOut'));
