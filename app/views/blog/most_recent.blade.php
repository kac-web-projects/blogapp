@extends('layouts.sidebar')
@section('title', 'Home')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Most Recent</h2>

<div class="row cell12">
	
	@if($posts->isEmpty())

		<div class="row">
			<h3>No posts created yet!</h3>
		</div>

	@else

		@foreach ($posts as $post)
		<div class="row">

			<h3><a href="{{ URL::to('post/' . $post->id) }}">{{ $post->post_title }}</a></h3>
			<div class = "padding5">{{ HTML::spaces(5) . '- By ' . $post->user->last_name . ', ' . $post->user->first_name . ' on ' . date("D, d M y", strtotime($post->created_at)) }}
					 ( {{ $post->postedAgo() }} )
			</div>
				<div class="image">
				<img src="{{ $post->image_path }}" style="width:550px;height:150px !important" />
			</div>
			<div class="readmore bg-grayLighter padding5 margin10 no-margin-left ">{{ $post->post_content }}</div>

		</div>
		@endforeach

	@endif
</div>

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		$('#posts').DataTable();
	});

</script>
@stop