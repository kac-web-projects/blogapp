@extends('layouts.default')
@section('title', 'ALL Posts')
@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> All Posts</h2>
<p>Click on row to View or Edit</p>
<div class="row cell12">

	<table id="posts" class="display" cellspacing="0" width="100%">
    
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Title</th>
	            <th>Content</th>
	            <th>Status</th>
	            <th>Author</th>
	            <th>Comment Status</th>
	            <th>Comment Count</th>
	        </tr>
	    </thead>

	    <tfoot>
	        <tr>
	            <th>#</th>
	            <th>Title</th>
	            <th>Content</th>
	            <th>Status</th>
	            <th>Author</th>
	            <th>Comment Status</th>
	            <th>Comment Count</th>
	        </tr>
	    </tfoot>

	    <tbody>
			@foreach ($posts as $key => $post)
			<tr id="{{ $post->id }}">
				<td> {{ $key + 1 }} </td>
				<td> {{ $post->post_title }} </td>
				<td class"plus-minus"> {{ $post->post_content }} </td>
				<td> {{ $post->post_status }} </td>
				<td> {{ $post->user->username }} </td>
				<td> {{ $post->comment_status ? 'Open' : 'Close' }} </td>
				<td> {{ $post->comment_count }} </td>
			</tr>
			@endforeach
	    </tbody>

	</table>
</div>

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		var table = $('#posts').DataTable({
			stateSave: true,
			lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});

		$('#posts tbody').on('click', 'tr', function () {
	        
	        var id = $(this).attr('id');
	        console.log(id);

	        window.open("{{ URL::to('post') }}/" + id);

	        // var data = table.row( this ).data();
	        // console.log(data);
	        // alert( 'You clicked on '+data[0]+'\'s row' );
    	} );

	});

</script>
@stop