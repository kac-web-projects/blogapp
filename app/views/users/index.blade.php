@extends('layouts.default')
@section('title', 'ALL Users')
@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"> </a> All Users</h2>

<div class="row cell12">

	<table id="users" class="display" cellspacing="0" width="100%">
    
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>First Name</th>
	            <th>Last Name</th>
	            <th>Username</th>
	            <th>Email</th>
	            <th>Active</th>
	            <th>Role</th>
	        </tr>
	    </thead>

	    <tfoot>
	        <tr>
	            <th>#</th>
	            <th>First Name</th>
	            <th>Last Name</th>
	            <th>Username</th>
	            <th>Email</th>
	            <th>Active</th>
	            <th>Role</th>
	        </tr>
	    </tfoot>

	    <tbody>
			@foreach ($users as $key => $user)
			<tr id="{{ $user->id }}">
				<td> {{ $key + 1 }} </td>
				<td> {{ $user->first_name }} </td>
				<td> {{ $user->last_name }} </td>
				<td> {{ $user->username }} </td>
				<td> {{ $user->email }} </td>
				<td> {{ $user->active }} </td>

				<!-- Relationship with Role model -->
				<td> {{ $user->role->name }} </td>
			</tr>
			@endforeach
	    </tbody>
	</table>

</div>

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		var table = $('#users').DataTable({
			stateSave: true,
			lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});

		$('#users tbody').on('click', 'tr', function () {
	        
	        var id = $(this).attr('id');
	        
	        window.open("{{ URL::to('user') }}/" + id);
    	} );

	});

</script>
@stop