<style type="text/css">
    /* Show only 4 lines in smaller screens */
    .articleKK {
        max-height: 7.2em;
        /*(4 * 1.5 = 6) */
        padding-bottom: 30px;
    }
</style>

<script type="text/javascript">

    var metroBlue = '#1ba1e2';
    var metroYellow = '#ffc40d';
    var metroRed = '#ee1111';

    $(document).ready(function() {

        //ReadMore
        $('.readmore').readmore({
            speed: 500,
            collapsedHeight: 126,
            heightMargin: 16,
            moreLink: '<a href="#">More</a>',
            lessLink: '<a href="#">less</a>'
        });

        var type       = "{{ Session::get('type') }}";
        var content    = "{{ Session::get('content') }}";
        var caption    = "{{ Session::get('caption') }}";
        var background = '';

        switch(type) {
            case 'Notify' :
            background = metroBlue;
            break;

            case 'Warning' :
            background = metroYellow;
            break;

            case 'Danger' :
            background = metroRed;
            break;
        }

        if (content != "") {
            $.Notify({style: {background: background, color: 'white'}, caption: caption, content: content});
        }

    });

//Event : Under Construction Elements 
$(document).on("click",".under-construction",function() {
    $.Notify({style: {background: metroYellow, color: 'white'}, caption: 'Feature', content: 'Under Construction'});
    return false;
});

</script>