<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class Services extends \BaseController {

	/**
	 * Delete Record from User, Posts & Roles in DB.
	 *
	 * @return string (custom) stored | updated | deleted |
	 * unauthorized | non-ajax
	 */
	public function delete() {
    	Log::info("Service::operations()");

		if (Request::ajax()) {
			
			if( Auth::check() ) {

				$in = Input::get('data');

					switch ($in['model']) {
						case 'user':
						Log::info("Delete => User; ID : " . $in['model']);
						User::destroy($in['id']);
						break;

						case 'post':
						Log::info("Delete => Post; ID : " . $in['model']);
						User::destroy($in['id']);
						break;

						case 'roles':
						Log::info("Delete => Roles; ID : " . $in['model']);
						User::destroy($in['id']);
						break;
					}

					return 'deleted'					
				}
			}
			else {
				Log::info("Operations => unauthorized");

				return 'unauthorized';
			}
    	}
    	else {
			Log::info("Operations => Non-Ajax Call");

    		return 'non-ajax';
    	}

	}

}