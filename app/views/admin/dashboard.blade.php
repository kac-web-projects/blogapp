@extends('layouts.default')
@section('title', 'Dashboard')

@section('content')

<div class="grid">

    <div class="row cells12" >
		
		<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Personal Dashboard</h2>

        <div class="row cell12">
        	
        	<h1>Posts</h1>
	            <div class="cell4">
		            <a href="{{ URL::to('post/create') }}" class="button primary"><span class="mif-plus">&nbsp;</span>Create</a>
		            <a href="{{ URL::to('myposts') }}" class="button primary">My Posts</a>
		            
		            @if($user->isAdmin())
		            <a href="{{ URL::to('post') }}" class="button primary">ALL Posts</a>
	        		@endif
	        	</div>

        	<h1>Comments</h1>
	
	        	 <div class="cell4">
		            <a href="{{ URL::to('mycomments') }}" class="button primary">My Discussion</a>
		            <a href="{{ URL::to('comments') }}" class="button primary">ALL Discussion</a>
	        	</div>

        	</div>


        	<h1>Users</h1>
	            <div class="cell4">
		            
		            @if($user->isAdmin())
			            <a href="{{ URL::to('user/create') }}" class="button primary"><span class="mif-plus">&nbsp;</span>Create</a>
			            <a href="{{ URL::to('active-users') }}" class="button primary">Active Users</a>
					@endif
		            
		            <a href="{{ URL::to('user') }}" class="button primary">ALL Users</a>
	        	</div>

            @if($user->isAdmin())
	        	<h1>Roles</h1>

				    <div class="cell4">
			            <a href="{{ URL::to('role/create') }}" class="button primary"><span class="mif-plus">&nbsp;</span>Add Role</a>
			            <a href="{{ URL::to('role') }}" class="button primary">ALL Role</a>
		        	</div>

	        @endif

            @if($user->isAdmin())
    
	        	<h1>Application Routes</h1>

				    <div class="cell4">
			            <a href="{{ URL::to('routes') }}" class="button primary">Routes</a>
		        	</div>
		    @endif

        </div>
	</div>

</div>
@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		// $('#posts').DataTable();
	});

</script>
@stop