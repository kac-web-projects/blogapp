# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: blog-web-app
# Generation Time: 2015-08-28 16:17:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Laravel will handle this',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Laravel will handle this',
  `post_id` varchar(16) NOT NULL COMMENT 'ID of the post for which the comment was made - Belongs to Posts Table',
  `user_id` bigint(20) unsigned NOT NULL COMMENT 'UserId belongs to Users table',
  `comment_author` bigint(20) unsigned NOT NULL COMMENT 'UserId belongs to Users table',
  `comment_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Content with markup',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Title',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Content with markup',
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish' COMMENT 'publish,unpublish,draft',
  `user_id` bigint(20) unsigned NOT NULL COMMENT 'UserId belongs to Users table',
  `image_path` varchar(200) DEFAULT NULL COMMENT 'Image-Media path if applicable',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open' COMMENT 'close,open,#',
  `comment_count` bigint(20) NOT NULL COMMENT 'Counter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `created_at`, `updated_at`, `post_title`, `post_content`, `post_status`, `user_id`, `image_path`, `comment_status`, `comment_count`)
VALUES
	(1,'2015-08-28 15:28:58','2015-08-28 16:00:22','World\'s fastest man taken out (Editing Now)','<p>Usain Bolt predicted that he wouldn&#39;t lose his favorite event at the world championships and, as is usually the case when the Jamaican steps onto a track, he went out and backed up his words.</p>\r\n\r\n<p>Bolt faced another showdown with American Justin Gatlin in the 200-meter final in Beijing on Thursday and did not blink as he captured his record 10th world title in a time of 19.55, the fifth fastest 200 ever.</p>\r\n\r\n<p>On Sunday, Bolt edged Gatlin in the 100-meter final by just one hundredth of a second. Both cruised to victories in Wednesday&#39;s semifinal heats to set up the rematch.</p>\r\n\r\n<p>&quot;Justin Gatlin was saying he was ready to go and was going to do something special. And for the 100 I don&#39;t mind people talking a lot because it&#39;s the 100 meters,&quot; Bolt said. &quot;But when it comes to my 200, I take it really personal.&quot;</p>\r\n\r\n<p>Gatlin came off the curve running step for step with Bolt, but that&#39;s when Bolt kicked it into another gear. Down the back stretch, he easily put distance between himself and Gatlin and coasted across the finish line all by himself to the world&#39;s best time of 2015.</p>\r\n','publish',1,'uploads/2015-08-27T141238Z_845242906_SR1EB8R13GO4O_RTRMADP_3_ATHLETICS-WORLD.jpg','1',0);

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Unique Role name',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Some Info',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'admin','All operations permitted'),
	(2,'author','Limitted operations permitted'),
	(3,'reader','Few operations permitted');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'FirstName' COMMENT 'FirstName of User',
  `last_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'LastName' COMMENT 'LastName of User',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Proper Email-Address for notification',
  `role_id` bigint(5) unsigned NOT NULL COMMENT 'RoleId belongs to Roles table',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column will be used to store a token for "remember me" sessions being maintained by your application.',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'To lock account - Future Scope',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `role_id`, `remember_token`, `active`)
VALUES
	(1,'media','$2y$10$k0Q7bYYFZiAdusCGpYwzMuV42kNYkw7zpEI/6tjzo1vQz0xxvz3e2','Media','Master','media@gmail.com',1,'Cj38iE8iu1YHaDtvjdX7F6xTmNugvJwxqHjRf1JZXUulJINPT6zT8m1Bb0Uj',1),
	(2,'norman','$2y$10$XNDV/.JGIxP3bINzIk038uXU7zZo1PndXFuuYpMQdUPJwqekgSX3G','Norman','Cote','norman@gmail.com',2,NULL,1),
	(3,'chris','$2y$10$q/QqvsrhstDzyVsT5t8ePegnodINbdqXClFgaqweoE9mPRXoY61bS','Chris','Roger','chris@gmail.com',3,NULL,1);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
