@extends('layouts.master')
@section('title', 'Logged-Out')

@section('content')

<div class="grid">

    <div class="row cells12">
		
		<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Logged-Out</h2>

        <div class="row">

	        <div class="row">
        		You are logged-out
        	</div>

	 		<a href="{{ URL::previous() }}" class="button primary">Back</a> 
	 		<a href="{{ URL::to('login') }}" class="button primary">Login Again</a> 

        </div>

	</div>

</div>
@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop






