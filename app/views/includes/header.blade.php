<div class="app-bar darcula" data-role="appbar">

    <a class="app-bar-element" href="{{ URL::to('/') }}">
        <span id="toggle-tiles-dropdown" class="mif-home mif-2x"></span>
    </a>

    <ul class="app-bar-menu">
        
        <li><a href="{{ URL::to('dashboard') }}" classK="under-construction">Dashboard</a></li>
        <li><a href="" class="under-construction">Messages</a></li>
        <li><a target="_blank" href="{{ URL::to('/') }}/manual/User.pdf">Help</a></li>
        <li><a href="" class="under-construction">SiteMap</a></li>
        
        @if( !Auth::check()) 
            <li><a href="" class="under-construction">Register</a></li>
            <li><a href="{{ URL::to('login') }}">Login</a></li>
        @endif
        
    </ul>

    <div class="app-bar-element place-right">
        <!-- Show UserName if Logged In -->
        @if(Auth::check())
            <a href="#" class="under-construction">Hi {{ ucfirst(User::where('id', Auth::id())->first()->username) }}</a>
        @else
            <a href="#" class="under-construction">Hi Guest</a>
        @endif


        <a class="dropdown-toggle fg-white"><span class="mif-settings-power mif-3x"></span> </a>
        
        <div class="app-bar-drop-container bg-white fg-dark place-right"
                data-role="dropdown" data-no-close="false">
        
            <div class="padding20">
                <form>
                    <div class="form-actions">
                        @if(Auth::check()) 
                            <a href="{{ URL::to('logout') }}" class="button warning">LogOut</a>
                        @else
                            <a href="{{ URL::to('login') }}" class="button warning">Login</a>
                        @endif

                        <a href="#" class="button bg-grayLight">Exit</a>
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>