@extends('layouts.default')
@section('title', 'New Role')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Add Role</h2>

<!--  Show Errors if applicable -->
<div class="row cells6">
	@if ($errors->has())

    <div class="block-shadow-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>        
        @endforeach
    </div>
    
    @endif
</div>

<div class="row cells6">
	{{ Form::open(array('url' => 'role')) }}

	<!--  Role Name -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('name','Role Name') }}

			<div class="input-control text full-size">
				{{ Form::text('name', null, array('placeHolder' => 'Name', 'class' => '')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>

			</div>
		</div>

	</div>

	<!--  Description -->
	<div class="row cells1">
	
		<div class="cell">
			{{ Form::label('description','Description') }}

			<div class="input-control textarea full-size">
				
				{{ Form::textarea('description', null, array('placeHolder' => 'Some Info' ,'class' => '')) }}
			</div>

		</div>

	</div>	



</div>
<!-- !cell8 -->

<div class="row cells3">

	<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
	{{ Form::submit('Create', array('class' => 'button primary')) }}

</div><!-- !cell4 -->

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop






