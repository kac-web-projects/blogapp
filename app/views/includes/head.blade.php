<meta charset="utf-8">
<meta name="description" content="">
<meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, software development, web developement, side project, cool tricks, custom stuff">
<meta name="author" content="Ketan Chawda">
<link rel='shortcut icon' type='image/x-icon' href="{{ URL::to('/') }}/My-Favicon.png" />

<title>Web Blog - @yield('title')</title>

<!-- Metro UI v3 CSS -->
<link href="{{ URL::to('/') }}/metro-v3/build/css/metro.css" rel="stylesheet" type="text/css">
<link href="{{ URL::to('/') }}/metro-v3/build/css/metro-icons.css" rel="stylesheet" type="text/css">
<link href="{{ URL::to('/') }}/metro-v3/build/css/metro-responsive.css" rel="stylesheet" type="text/css">
<link href="{{ URL::to('/') }}/metro-v3/build/css/metro-schemes.css" rel="stylesheet" type="text/css">
<link href="{{ URL::to('/') }}/metro-v3/build/css/metro-schemes.css" rel="stylesheet" type="text/css">
<link href="{{ URL::to('/') }}/metro-v3/iconFont.css" rel="stylesheet" type="text/css">

<!-- JQuery, UI Js + Css -->
<script src="{{ URL::to('/') }}/jquery/jquery-2.1.3.min.js"></script>

<!-- DataTables CSS -->
<link href="{{ URL::to('/') }}/dataTables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="{{ URL::to('/') }}/dataTables/media/js/jquery.js"></script>

<!-- DataTables Js -->
<script src="{{ URL::to('/') }}/dataTables/media/js/jquery.dataTables.min.js"></script>

<!-- Metro UI v3 Js -->
<script src="{{ URL::to('/') }}/metro-v3/build/js/metro.js"></script>

<!-- CkEditor Js -->
<script src="{{ URL::to('/') }}/js/ckeditor/ckeditor.js"></script>

<!-- ReadMore Js -->
<script src="{{ URL::to('/') }}/js/readmore/readmore.js"></script>