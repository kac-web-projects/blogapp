@extends('layouts.default')
@section('title', 'Application Routes')

@section('content')
		
<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> All Routes</h2>

<div class="row cell12">

	<table id="routes" class="display" cellspacing="0" width="100%">
    
        <thead>
            <tr>
                <th>#</th>
                <th>Path</th>
                <th>Method</th>
                <th>Action Name</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th>#</th>
                <th>Path</th>
                <th>Method</th>
                <th>Action Name</th>
            </tr>
        </tfoot>

        <tbody>
    		@foreach ($routes as $key => $route)
    		<tr>
    			<td> {{ $key + 1 }} </td>
    			<td> {{ $route->getPath() }} </td>
    			<td> {{ $route->getMethods()[0] }} </td>
    			<td> {{ $route->getActionName() }} </td>
    		</tr>
    		@endforeach
        </tbody>
    </table>

</div>

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		var table = $('#routes').DataTable({
			stateSave: true,
			lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});

	});

</script>
@stop