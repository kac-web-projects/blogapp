<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class CommentService extends \BaseController {

	/**
	 * List/Store/Update/Delete Comment on Post in DB.
	 *
	 * @return string (custom) stored | updated | deleted |
	 * unauthorized | non-ajax
	 */
	public function operations() {
    	Log::info("CommentService::operations()");

		if (Request::ajax()) {
			
			$in = Input::get('data');
			// Log::debug($in);
			
			//Note : No Authentication required
			if($in['type'] == 'list') {

				Log::info("Operations => list");

		        // store
	            $comments = Comment::where('post_id', $in['postId'])->get();
	            Log::debug($comments->toArray());
				foreach ($comments as $key => $comment) {
					$user = User::find($comment->user_id)->first();
					$comment->author = $user->last_name . ', ' . $user->first_name;
					$comment->ago = self::ago(strtotime($comment->created_at));
				}

				return $comments;
				
			}

			//Note : Authentication required
			if( Auth::check() ) {

				if($in['type'] == 'store') {

    				Log::info("Operations => store");

			        // store
		            $comment                  = new Comment();
		            $comment->post_id         = $in['postId'];
		            $comment->user_id         = Auth::id();
		            $comment->comment_content = $in['postComment'];
		            $comment->save();

					return 'stored';
				}
				else if($in['type'] == 'update') {

    				Log::info("Operations => update");

			        // update
		            $comment                  = Comment::find($in['id']);
		            $comment->user_id         = Auth::id();
		            $comment->comment_content = $in['comment'];
		            $comment->save();

					return 'updated';
				}
				else if($in['type'] == 'delete') {
    				
    				Log::info("Operations => delete");

    				//delete
					Comment::destroy($in['id']);

					return 'deleted';
				}
				
			}
			else {
				Log::info("Operations => unauthorized");

				return 'unauthorized';
			}
    	}
    	else {
			Log::info("Operations => Non-Ajax Call");

    		return 'non-ajax';
    	}

	}

	//Ago Functionality
	public static function ago($time)
	{
	   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	   $lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

	       $difference     = $now - $time;
	       $tense         = "ago";

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	       $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
	       $periods[$j].= "s";
	   }

	   return "$difference $periods[$j] ago ";
	}

}