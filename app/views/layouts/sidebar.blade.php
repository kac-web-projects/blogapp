<!doctype html>
<html>
<head>
    @include('includes.head')
</head>

<body class="metro">

    <header class="bg-dark fg-light" data-load="header.html">
        @include('includes.header')
    </header>

    <div class="container">

        <div class="grid">

            <div class="row cells12">

                <!-- sidebar content -->
                <div id="sidebar" class="cell colspan3">
                    @include('includes.left-sidebar')
                </div>

                <!-- main content -->
                <div id="content" class="cell colspan7">
                    @yield('content')
                </div>

            </div><!-- !row cells12 -->

            <div class="row">

                <footer class="row">
                    @include('includes.footer')
                </footer>
            
            </div><!-- !row cells12 -->
        
        </div><!-- grid -->

    </div><!-- container -->
   
    @include('includes.script')
    <!-- JS Section -->
    @yield('script')

</body>
</html>