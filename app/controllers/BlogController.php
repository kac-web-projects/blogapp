<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class BlogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		Log::info("BlogController::index()");

		//Note : Filter Only Published Post
		$posts = Post::where('post_status','publish')->get();

		return View::make('blog.index')->with('posts',$posts);
	}


	/**
	 * Display a Most Commented Posts
	 *
	 * @return Response
	 */
	public function mostCommented()
	{
		Log::info("BlogController::mostCommented()");

		//TODO : Order by Comment-Count
		//Note : Filter Only Published Post
		$posts = Post::where('post_status','publish')
						->orderBy('comment_count','desc')->get();

		return View::make('blog.most_commented')->with('posts',$posts);
	}

	/**
	 * Display a Most Popular Posts
	 *
	 * @return Response
	 */
	public function mostPopular()
	{
		Log::info("BlogController::mostPopular()");

		//TODO-Scope : Order by Popular-Count
		//Note : Filter Only Published Post
		$posts = Post::where('post_status','publish')->get();

		return View::make('blog.most_popular')->with('posts',$posts);
	}

	/**
	 * Display a Most Recent Posts
	 *
	 * @return Response
	 */
	public function mostRecent()
	{
		Log::info("BlogController::mostRecent()");

		//Note : Filter Only Published Post & Ordered by Created_at
		$posts = Post::where('post_status','publish')
						->orderBy('created_at','desc')->get();

		return View::make('blog.most_recent')->with('posts',$posts);
	}

	/**
	 * Defining A Catch-All Method
	 *
	 * @param  Request Array
	 * @return Response
	 */
	public function missingMethod($parameters = array())
	{
		Log::info("BlogController::missingMethod()");
		
		return 'missingMethod';
	}

}