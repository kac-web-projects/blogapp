@extends('layouts.default')
@section('title', 'New Post')

@section('content')
		
<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Add New Post</h2>

<div class="row cells6">
	{{ Form::open(array('url' => 'post','files'=>true)) }}

	<!--  Post Title -->
	<div class="row cells1">

		<div class="cell">
			{{ Form::label('post_title','Title') }}

			<div class="input-control text full-size">

				{{ Form::text('post_title',null,array('placeHolder' => 'What\'s on your mind','class' => '')) }}
			</div>

		</div>
	</div>

	<!--  Post Content -->
	<div class="row cells1">
	
		<div class="cell">
			{{ Form::label('post_content','Content') }}

			<div class="input-control textarea full-size">
				
				{{ Form::textarea('post_content',null,array('placeHolder' => 'What\'s on your mind' ,'class' => 'ckeditor')) }}
			</div>

		</div>

	</div>	

	<!--  Media -->
	<div class="row cells2">

		<div class="cell">

			{{ Form::label('image', 'Upload Image')}}
			<div class="input-control text full-size">

				{{ Form::file('image') }}
			</div>

		</div>
	</div>

	<!--  Status -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('post_status', 'Post Status')}}
			
			<div class="input-control select full-size">
				
				{{ Form::select('post_status',['publish' => 'Publish', 'unpublish' => 'unpublish', 'draft' => 'draft'])}}
			</div>

		</div>
	</div>

	<!-- Comment ON/OFF -->
	<div class="row cells2">
	
		<div class="cell">
			
			<label class="switch"> Allow Comment {{ HTML::spaces(2) }}

				{{ Form::checkbox('comment_status',0)}}
			    <span class="check"></span>
			</label>

		</div>
	</div>
	</div>
	
	<!-- !cell8 -->

	<div class="row cells4">

		<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
		{{ Form::submit('Post', array('class' => 'button primary')) }}

	</div><!-- !cell4 -->

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		// $.get("URL::to('/') }}",{
		// });

	});

</script>
@stop






