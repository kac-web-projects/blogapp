<!-- sidebar nav -->
<ul class="sidebar2 dark" style="margin-top: 60px">

	<li class="title">Quick Links</li>
	
	<li class="#">
		<a href="{{ URL::to('most-recent') }}">
			<span class="mif-cog icon"></span>
			Most Recent
		</a>
	</li>

	<li class="#">
		<a href="{{ URL::to('most-commented') }}">
			<span class="mif-cog icon"></span>
			Most Commented
		</a>
	</li>

	<li class="#">
		<a href="{{ URL::to('most-popular') }}">
			<span class="mif-cog icon"></span>
			Most Popular
		</a>
	</li>

	<li class="#">
		<a href="#" class="under-construction">
			<span class="mif-cog icon"></span>
			Filter By Date
		</a>
	</li>

	<li class="#">
		<a href="#" class="under-construction">
			<span class="mif-cog icon"></span>
			Featured
		</a>
	</li>

	<li class="#">
		<a href="#" class="under-construction">
			<span class="mif-cog icon"></span>
			Sports
		</a>
	</li>

	<li class="#">
		<a href="{{ URL::to('post/create') }}">
			<span class="mif-cog icon"></span>
			Create Post
		</a>
	</li>

	
</ul>