@extends('layouts.default')
@section('title', 'Edit Post')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Edit Post</h2>

<div class="row cell colspan8 offset2">

    	{{ Form::model($post, array('route' => array('post.update', $post->id),'method' => 'PATCH')) }}    

			<!--  Post Title -->
			<div class="row cells1">

				<div class="cell">

					{{ Form::label('post_title','Title') }}

					<div class="input-control text full-size">
					
					     {{ Form::text('post_title') }}
					</div>

				</div>
			</div>

			<!--  Post Content -->
			<div class="row cells1">
	
				<div class="cell">
			
					{{ Form::label('post_content','Content') }}

					<div class="input-control textarea full-size">

						{{ Form::textarea('post_content', null,array('class' => 'ckeditor')) }}
					</div>

<!-- 				<div class="article">
				</div>
 -->
				</div>	
			</div>	

			<!--  Media -->
			<div class="row cells2">

				<div class="cell">

					{{ Form::label('image', 'Upload Image')}}
					<div class="input-control text full-size">

						{{ Form::file('image') }}
					</div>

				</div>
			</div>


			<!--  Status -->
			<div class="row cells2">

				<div class="cell">
				{{ Form::label('post_status', 'Post Status')}}

					<div class="input-control select full-size">
				
					{{ Form::select('post_status',['publish' => 'Publish', 'unpublish' => 'unpublish', 'draft' => 'draft'])}}

					</div>
			
				</div>
			</div>

			<!-- Comment ON/OFF -->
			<div class="row cells2">
			
				<div class="cell">
					
					<label class="switch"> Allow Comment {{ HTML::spaces(2) }}

						{{ Form::checkbox('comment_status',1)}}
					    <span class="check"></span>
					</label>

				</div>
			</div>

			</div>

	@if($currentUser)
		@if($currentUser->isAdmin() || $currentUser->isOwner('Post', $post->id))
		<div class="row cells4">

			<!-- Scope : Share the post -->
			<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
			{{ Form::submit('Update', array('class' => 'button primary')) }}

		</div><!-- !cell4 -->
		@endif
	@endif

	

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop