<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	$date = new DateTime();
	Log::info('URL requested : '. Request::url() . ' @ ' .  $date->format('Y-m-d H:i:s'));
});


App::after(function($request, $response)
{
	$date = new DateTime();
	Log::info('URL feeded : '. Request::url() .  '@ ' . $date->format('Y-m-d H:i:s'));
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	$date = new DateTime();
	Log::info("Authentication Check @ : ". $date->format('Y-m-d H:i:s'));

	if (Auth::guest()) {
		
		if (Request::ajax()) {
			
			return Response::make('Unauthorized', 401);
		}
		else {

			Log::info("Guest User, Redirecting to Login");
			
			Session::flash('type', 'Danger');
	        Session::flash('caption', 'Access');
	        Session::flash('content', 'Need Authenticated User');

			return Redirect::guest('login');
		}
	}

});


/*
|--------------------------------------------------------------------------
| Admin Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth.admin', function()
{
	$date = new DateTime();
	Log::info("Admin Check @ : ". $date->format('Y-m-d H:i:s'));

	if (Auth::check()) {

		$user = User::findOrFail(Auth::id());

		if( !$user->isAdmin()) {

			Session::flash('type', 'Danger');
	        Session::flash('caption', 'Admin');
	        Session::flash('content', 'Access Denied - Need Admin User');
		
			return View::make('errors.401');
		} 
		else {

		}


	}
	else {

		Log::info("Guest User, Redirecting to Login");
		return Redirect::guest('login');
	}

});

//Usage : To check if User has permission to CREATE Post, Comment
Route::filter('auth.allowed', function()
{
	$date = new DateTime();
	Log::info("Permission Check @ : ". $date->format('Y-m-d H:i:s'));

	Session::flash('type', 'Danger');
    Session::flash('caption', 'Admin');
    Session::flash('content', 'Test');

	// if (Auth::check()) {

	// 	$user = User::findOrFail(Auth::id());

	// 	if( !$user->isAdmin()) {

	// 		Session::flash('type', 'Danger');
	//         Session::flash('caption', 'Admin');
	//         Session::flash('content', 'Access Denied');
		
	// 		return View::make('errors.401');
	// 	}

	// }
	// else {

	// 	Log::info("Guest User, Redirecting to Login");
	// 	return Redirect::guest('login');
	// }

});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
