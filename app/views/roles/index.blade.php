@extends('layouts.default')
@section('title', 'ALL Roles')
@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> All Roles</h2>

<div class="row cell12">

	<table id="roles" class="display" cellspacing="0" width="100%">
    
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Role Name</th>
	            <th>Description</th>
	        </tr>
	    </thead>

	    <tfoot>
	        <tr>
	            <th>#</th>
	            <th>Role Name</th>
	            <th>Description</th>
	        </tr>
	    </tfoot>

	    <tbody>
			@foreach ($roles as $key => $role)
			<tr id="{{ $role->id }}">
				<td> {{ $key + 1 }} </td>
				<td> {{ $role->name }} </td>
				<td> {{ $role->description }} </td>
			</tr>
			@endforeach
	    </tbody>
	</table>

</div>

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		var table = $('#roles').DataTable({
			stateSave: true,
			lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});

		$('#roles tbody').on('click', 'tr', function () {
	        
	        var id = $(this).attr('id');
	        
	        window.open("{{ URL::to('role') }}/" + id + '/edit');
    	} );


	});

	function deleteRole() {


	    $.get("{{ URL::to('services/delete') }}", { data : { 'model' : 'roles', 'id' : id } },function(status) {

	        if(status == 'deleted') {
	        
	            $.Notify({style: {background: metroBlue, color: 'white'}, caption: 'Comment', content: 'Deleted Successfully'});
	        }
	        else if(status == 'unauthorized') {
	        
	            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access Denied', content: 'Please Login First'});
	        }
	        else if(status == 'non-ajax') {
	        
	            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Operation', content: 'Not permitted'});
	        }
	        else {
	        
	            $.Notify({style: {background: metroYellow, color: 'white'}, caption: 'Try Again', content: 'Something went Wrong'});
	        }
   		
   		});

	}

</script>
@stop