<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class CommentController extends \BaseController {

	/**
	 * Listing Individual Posts
	 *
	 * @return Response
	 */
	public function userComments()
	{
		Log::info("CommentController::userComments()");

		return 'TODO';
	}

	/**
	 * Defining A Catch-All Method
	 *
	 * @param  Array
	 * @return Response
	 */
	public function missingMethod($parameters = array())
	{
		Log::info("CommentController::missingMethod()");
		
		return 'missingMethod';
	}

}