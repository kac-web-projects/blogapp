@extends('layouts.default')
@section('title', 'Edit Role')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Edit Role</h2>

<!--  Show Errors if applicable -->
<div class="row cells6">
	@if ($errors->has())

    <div class="block-shadow-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>        
        @endforeach
    </div>
    
    @endif
</div>

<div class="row cells6">
	{{ Form::model($role, array('route' => array('role.update', $role->id), 'method' => 'PATCH')) }}
	
	<!--  Role Name -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('name','Role Name') }}

			<div class="input-control text full-size">
				{{ Form::text('name', null, array('placeHolder' => 'Name', 'class' => '')) }}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>

			</div>
		</div>

	</div>

	<!--  Description -->
	<div class="row cells1">
	
		<div class="cell">
			{{ Form::label('description','Description') }}

			<div class="input-control textarea full-size">
				
				{{ Form::textarea('description', null, array('placeHolder' => 'Some permission' ,'class' => '')) }}
			</div>

		</div>

	</div>	


</div>
<!-- !cell8 -->

<div class="row cells4">

    <div class="row cell colspan4 offset5" style="margin-top: 50px">
		<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
 		{{ Form::submit('Update', array('class' => 'button primary')) }}
	</div>

</div><!-- !cell4 -->

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop