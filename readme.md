
---------------------------------------------

### Sample Project - Under construction
A simple blog web app using whatever Laravel, frameworks and testing suites you wish.

## Feature

# Users
a) Users can view a list of readers or authors
b) Readers cannot create new posts
c) Edit own profile
d) Sign-up

# Posts
a) Posts can be created, updated and deleted by owner
b) Posts can be read by anyone
c) Posts can be commented on

#  Comments
a) Newly created comments by poster appear without post page without reloading
b) Comments can be updated and deleted by comment author
c) Comments can be deleted by authors

# Blog
a) Displays a list of all posts, categorized by date

#  Authentication
a) Security

#  Roles Mgt
a) Add roles, Edit roles, Delete Roles


#  Dashboard
a) Based on roles such as User Mgt, Post monitoring, Roles Mgt and more

More as Scope

Writing more custom code
Using MVC structure provided by framework

---------------------------------------------


## Web Blog Technical Overview

## 1) docs FOLDER
Ex : $ /Users/ketanchawda/Documents/mylaravel/blog-web-app/docs

1.1 DB dump : blog-web-app_2015-08-28.sql
	1.1.1 user : root; password : root
1.2 Demo Roles & Users : demoData.sql
1.3 Schema if required : schema.sql
1.4 Technical-Guide.pdf : Under construction
1.5 User-Guide.pdf : Under construction

## 2 ) guide FOLDER : 
Ex : $ guide

Have Snapshots & Steps for System Overview/Functioning

## 3) Download/Install Laravel
3.1 http://laravel.com/docs/4.2


## 4) Start Server
$ cd blog-web-app/public;
$ php -S 0.0.0.0:1111 ../server.php;

OR 

php artisan serve

Output :

PHP 5.6.2 Development Server started at Fri Aug 28 09:45:01 2015
Listening on http://0.0.0.0:1111
Document root is /Users/ketanchawda/Documents/mylaravel/blog-web-app/public
Press Ctrl-C to quit.

Visit http://localhost:1111

##5) Trace Laravel Log file
4.1 $ cd blog-web-app/app/storage/logs;
4.2 $ tail -f laravel.log;

5) More commands
5.1 $ php artisan cache:clear;
5.2 $ php artisan dump-autoload;

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
