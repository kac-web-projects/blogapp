<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

class Post extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';


	/**
	 * Timestamps field such as created_at and updated_at if required.
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * Relationship with User Table (One to Many)
	 */
	public function user()
    {
        return $this->hasOne('User','id','user_id');
    }

	public function postedAgo()
    {
    	return self::ago(strtotime($this->created_at));
    }
	
	//Ago Functionality
	public static function ago($time)
	{
	   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	   $lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

	       $difference     = $now - $time;
	       $tense         = "ago";

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	       $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
	       $periods[$j].= "s";
	   }

	   return "$difference $periods[$j] ago ";
	}

	/**
	 * List Comments field such as created_at and updated_at if required.
	 *
	 * @return HTML
	 */
	public function listComments() {

		return 'Comments with link and short description';
	}

}
