@extends('layouts.default')
@section('title', 'View Post')

@section('content')
		
<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> View Post

	<!-- Allow Edit if Adming or Owner -->
	@if($currentUser)
		@if($currentUser->isAdmin() || $currentUser->isOwner('Post', $post->id))
			<a href="{{ URL::to('post/' . $post->id . '/edit') }}" class="mif-pencil place-right bg-white fg-black"></a>
		@endif
	@endif

</h2>

<div class="row cell colspan8 offset2">

    	{{ Form::model($post, array('route' => 'post.edit', $post->id,'method' => 'PUT')) }}    

			<!--  Post Title -->
			<div class="row cells1">

				<div class="class bg-grayLighter padding10">
					{{ $post->post_title }}
				</div>

			</div>


			<!--  Post Ago -->
			<div class="row cells1">
				<div class="padding5">{{ HTML::spaces(5) . '- By ' . $post->user->last_name . ', ' . $post->user->first_name . ' on ' . date("D, d M y", strtotime($post->created_at)) }}
					 ( {{ $post->postedAgo() }} )
				</div>
			</div>

			<!--  Post Content -->
			<div class="row cells1">

				<div class="readmore bg-grayLighter padding10">{{ $post->post_content }}</div>
			</div>	


			<!-- Comment ON/OFF -->
			<div class="row cells2">

				@if($post->comment_status)
					Comment : Open
				@else
					Comment : Closed
				@endif

			</div>

	<!-- Add Comment-->
	<div class="row full-size">
		<img src="{{ URL::to('/') }} /images/profile.png"/>
		
		<div class="input-control textarea full-size">

			{{ Form::textarea('comment', null,array('id' => 'comment', 'class' => '','rows' => '3', 'colspan' => '30','placeHolder' => 'Start the discussion')) }}
			
			<!-- Show/Hide Comment Controller-->
    		<button class="button warning" id="commentController">
    			<i class="mif-bubbles">&nbsp; Show Comments</i>
    		</button>

			<a class="button primary on-right operations" type="store">
				<i class="mif-bubble">&nbsp; Add</i>
			</a>

		</div>
		
	</div>


	<!-- Allow Comment if Logged In -->

	<!-- Comment Section if Applicable-->
	<div class="row cells2" id="commentSection" style="display:none">
		<!-- Dynamically Loaded -->
	</div>

</div>

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	//Globals : Require for Ajax Calls
    var postId  = "{{ $post->id }}";
    var commentAllowed = "{{ $post->comment_status }}";

    //Admin user, Track user
    var isAdmin = false;
    var currentUser = null;

	// Allow Edit if Adming or Owner -->
	@if($currentUser)
    	isAdmin = "{{ $currentUser->isAdmin() }}";
    	currentUser = "{{ Auth::id() }}";
	@endif
 
	$(document).ready(function() {

		$(document).on('click', '.operations', function() {

			var type = $(this).attr('type');

			switch(type) {
				case 'store':
				storeComment();

				break;

				case 'update':
				var commentId = $(this).attr('commentId');
				var newContent = $('#' + commentId).val();
				var r = confirm("Update Comment ?");
				if (r == true) {
					updateComment(commentId, newContent);
				}

				break;

				case 'delete':
				var r = confirm("Delete Comment ?");
				if (r == true) {
					deleteComment($(this).attr('commentId'));
				}
				break;
			}

			listComment();

			return false;
		});

		//Show/Hide Comments
		$(document).on('click', '#commentController', function() {

        	if ($('#commentSection').is(":visible")) {

            	$('#commentSection').hide();
            	$('#commentController').html("<i class='mif-bubbles'>&nbsp; Show Comments</i>");
        	}
        	else {
				listComment();

            	$('#commentSection').show();
            	$('#commentController').html("<i class='mif-bubbles'>&nbsp; Hide Comments</i>");
        	}
		
			return false;
        });


	});

//Get Comments for Post
function listComment() {

    $.get("{{ URL::to('comment/operations') }}", { data : { 'type' : 'list', 'postId' : postId } }, function(status) {

        if($.isArray(status)) {
            updateList(status);
            // $.Notify({style: {background: metroBlue, color: 'white'}, caption: 'Comment', content: 'Listed Successfully'});
        }
        else if(status == 'unauthorized') {
        
            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access Denied', content: 'Please Login First'});
        }
        else if(status == 'non-ajax') {
        
            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Operation', content: 'Not permitted'});
        }
        else {
        
            $.Notify({style: {background: metroYellow, color: 'white'}, caption: 'Try Again', content: 'Something went Wrong'});
        }
    });

}

//Update DOM
function updateList(comments) {
    // console.log(comments);
    
    var html = "";
    $.each(comments, function(key, comment) {
    	var commentId = comment['id'];
        html += "<br/><div class='row full-size'>";
        html += "<br/>By " + comment['author'] + " " + comment['ago'];

        //Controls
        if(isAdmin || currentUser == comment['user_id']) {
        	html += "<br><textarea cols='60' rows='3' id='" + commentId + "'>" + comment['comment_content'] + "</textarea>";
            
            //Edit, Delete
            html += "<br/><a type='update' class='operations' commentId='" + commentId + "'><i class='mif-pencil'></i></a>&nbsp;&nbsp";
            html += "<a type='delete' class='operations' commentId='" + commentId + "''><i class='mif-cross'></i></a>";
        }
        else {
        	html += "<br><textarea disabled cols='60' rows='3' id='" + commentId + "'>" + comment['comment_content'] + "</textarea>";
        }
    
        html += "</div>";

    });

    $('#commentSection').html(html);
}

//store
function storeComment() {

	if( !isAdmin && !commentAllowed) {
	
		$.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access', content: 'Denied, since Comment is Closed'});
	}
	else {
		
	    var postComment = $("#comment").val();

		if($('#comment').val() != '') {

		    $.get("{{ URL::to('comment/operations') }}", { data : { 'type' : 'store', 'postId' : postId, 'postComment' : postComment} },function(status) {

		        if(status == 'stored') {
		            listComment();
		            
		            $('#comment').val('');
		            $.Notify({style: {background: metroBlue, color: 'white'}, caption: 'Comment', content: 'Added Successfully'});
		        }
		        else if(status == 'unauthorized') {
		        
		            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access Denied', content: 'Please Login First'});
		        }
		        else if(status == 'non-ajax') {
		        
		            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Operation', content: 'Not permitted'});
		        }
		        else {
		        
		            $.Notify({style: {background: metroYellow, color: 'white'}, caption: 'Try Again', content: 'Something went Wrong'});
		        }
		    });

		}
		else {

			$.Notify({style: {background: metroRed, color: 'white'}, caption: 'Comment', content: 'Can\'t Add Empty'});
		}
	}


}

//Update
function updateComment(commentId, newContent) {

    $.get("{{ URL::to('comment/operations') }}", { data : { 'type' : 'update', 'id' : commentId ,'comment' : newContent  } },function(status) {

        if(status == 'updated') {

            $('#comment').val('');
        
            $.Notify({style: {background: metroBlue, color: 'white'}, caption: 'Comment', content: 'Updated Successfully'});
        }
        else if(status == 'unauthorized') {
        
            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access Denied', content: 'Please Login First'});
        }
        else if(status == 'non-ajax') {
        
            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Operation', content: 'Not permitted'});
        }
        else {
        
            $.Notify({style: {background: metroYellow, color: 'white'}, caption: 'Try Again', content: 'Something went Wrong'});
        }
    });

}

//Delete 
function deleteComment(commentId) {

	if( !isAdmin && !isAuthor) {
	
		$.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access', content: 'Denied, since Comment is Closed'});
	}
	else {

	    $.get("{{ URL::to('comment/operations') }}", { data : { 'type' : 'delete', 'id' : commentId } },function(status) {

	        if(status == 'deleted') {
	        
	            $.Notify({style: {background: metroBlue, color: 'white'}, caption: 'Comment', content: 'Deleted Successfully'});
	        }
	        else if(status == 'unauthorized') {
	        
	            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Access Denied', content: 'Please Login First'});
	        }
	        else if(status == 'non-ajax') {
	        
	            $.Notify({style: {background: metroRed, color: 'white'}, caption: 'Operation', content: 'Not permitted'});
	        }
	        else {
	        
	            $.Notify({style: {background: metroYellow, color: 'white'}, caption: 'Try Again', content: 'Something went Wrong'});
	        }
	    });
   	}
}

</script>
@stop