@extends('layouts.default')
@section('title', 'View User')
@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> View User
<span class="place-right"><a href="{{ URL::to('user/' . $user->id . '/edit') }}" class='mif-pencil'></a></span></h2>

<div class="row cells6">
	{{ Form::model($user, array('route' => 'user.update', $user->id)) }}
	
	<!--  UserName -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('username','Username') }}

			<div class="input-control text full-size">
				{{ Form::text('username',null,array('classK' => 'mif-2x', 'disabled')) }}
			</div>

		</div>
	</div>

	<!--  First Name -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('first_name','First Name') }}

			<div class="input-control text full-size">
				{{ Form::text('first_name',null,array('classK' => 'mif-2x','disabled')) }}
			</div>

		</div>
	</div>


	<!--  Last Name -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('last_name','Last Name') }}

			<div class="input-control text full-size">
				{{ Form::text('last_name',null,array('placeHolder' => 'Last name','class' => '','disabled')) }}
			</div>
	
		</div>
	</div>

	<!--  Email -->
	<div class="row cells2">
		
		<div class="cell">
			{{ Form::label('email','Email') }}

			<div class="input-control select full-size">
				{{ Form::text('email',null,array('placeHolder' => 'Email','class' => '','disabled')) }}
				
			</div>

		</div>
	</div>

	<!--  User Role -->
	<div class="row cells2">

		<div class="cell">
			{{ Form::label('role_id', 'Role')}}
			
			<div class="input-control select full-size">
				
				{{ Form::select('role_id', $roles, array('disabled'))}}
			</div>

		</div>
	</div>

	<!--  Active -->
	<div class="row cells2">
		
		<label class="input-control checkbox full-check ">
		    <input type="checkbox" checked disabled>
		    <span class="caption">Active</span>
		    <span class="check"></span>
		</label>
	</div>

</div>

</div>
<!-- !cell8 -->

{{ Form::close() }}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop