@extends('layouts.default')
@section('title', 'Login')

@section('content')

<div class="grid">

    <div class="row cells12">


	<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Login</h2>

	<div class="row cells12">

	{{ Form::open(array('action' => array('LoginController@doLogin'))) }}

		<!--  Username -->
		<div class="row cells2">

			<div class="cell">
				{{ Form::label('username', 'Username') }}

				<div class="input-control text full-size" datad-role="input-control">
					{{ Form::text('username', null, array('placeHolder' => 'Username || Email')) }}

					<button class="button helper-button clear" onClick="return false">
						<span class="mif-cross"></span>
					</button>

				</div>

			</div>

		</div><!-- !cell6 -->

		<!--  Password -->
		<div class="row cells3">

			<div class="cell">
				{{ Form::label('password', 'Password') }}

				<div class="input-control text full-size">
					{{ Form::password('password', null, array('class' => '')) }}
					
					<button class="button helper-button clear" onClick="return false">
						<span class="mif-cross"></span>
					</button>

				</div>

			</div>

		</div><!-- !cells3 -->

		<!--  Remember Me -->
		<div class="row cells2">
			
			<label class="input-control checkbox small-check">
			    <input type="checkbox" name="remember_me">
			    <span class="check"></span>
			    <span class="caption">Remember me</span>
			</label>
		</div>


	</div>
	<!-- !cell12 -->

	<div class="row cells4">

		<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
		{{ Form::submit('Proceed', array('class' => 'button primary')) }}

	</div><!-- !cell4 -->


</div>
	{{ Form::close() }}
@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop






