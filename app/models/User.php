<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	
	protected $fillable = array('email', 'username', 'password');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Timestamps field such as created_at and updated_at if required.
	 *
	 * @var boolean
	 */
	public $timestamps = false;


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	// protected $hidden = array('password');

	public function getAuthIdentifier() {
        
        return $this->getKey();
  	}

	public function getAuthPassword() {
        
        return $this->password;
    }

    public function getRememberToken() {
	   
	   return $this->remember_token;
	}

	public function setRememberToken($value) {
		
		$this->remember_token = $value;
	}

	public function getRememberTokenName() {
		
		return 'remember_token';
	}

	/**
	 * Relationship with Roles Table (One to One)
	 */
	public function role()
    {
        return $this->hasOne('Roles','id','role_id');
    }


	/**
	 * Check if User has Admin Role
	 *
	 * @return boolean
	 */
    public function isAdmin() {

		if(Str::lower($this->role->name) == 'admin') {
			
			Log::info("User is Admin");
			return true;
		}

		return false;
	}

	/**
	 * Check if User has Author Role
	 *
	 * @return boolean
	 */
    public function isAuthor() {

		if(Str::lower($this->role->name) == 'author') {
			
			Log::info("User is Author");
			return true;
		}
			
		return false;
	}

	/**
	 * Check if User is Owner of Post, Comment
	 *
	 * @return boolean
	 */
    public function isOwner($model, $recordId) {

		Log::info("isOwner check. Model : $model, ID : $recordId");
    	
    	$flag = false;

    	switch ($model) {
    		case 'Post':
    			$post = Post::find($recordId)->first()->user_id;
    			$flag = (Auth::id() == $post) ? 1 : 0;
    			break;
    		
    		case 'Comment':
    			$comment = Comment::find($recordId)->first()->user_id;
    			$flag = (Auth::id() == $comment) ? 1 : 0;
    			break;
    	}

		Log::info("isOwner check ? $flag");
		return $flag;
	}


	/**
	 * Role Name via Relationship 
	 *
	 * @return String
	 */
    public function getRoleName() {

		return Str::lower($this->role->name);
	}

}